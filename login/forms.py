from django import forms
from django.contrib.auth import models
from django.forms import widgets
import unicodedata
from django.utils.text import capfirst
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)

UserModel = get_user_model()


#
class UserCreation(UserCreationForm):
    """class for the form create a user"""


    password1 = forms.CharField(
        min_length=10,
        label=_("Password"),
        strip=False,
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'placeholder':'Contraseña'}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        min_length=10,
        label=_("Password confirmation"),
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'password', 'placeholder':'Confirmar Contraseña'}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.EmailField(required=True)


    """Meta class, which is responsible for loading the model you want to use
      and the fields you want to use for the form """
    class Meta:
        model = User
        
        fields = [
            'username',
            'first_name',
            'last_name',
            'email'
        ]
        labels ={
            'username': 'username',
            'first_name': 'first_name',
            'last_name': 'last_name',
            'email': 'email',
        }
        widgets = {
            'email':forms.TextInput(attrs={'class':'form-control','id':'field-5', 'placeholder':'email'}),
        }


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
    )

    error_messages = {
        'invalid_login': _(
            "Por favor ingrese un nombre de usuario y una contraseña conrrecta. Recuerde que los campos son sensibles a las mayúsculas."
        ),
        'inactive': _("This account is inactive."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

        # Set the label for the "username" field.
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        return username
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache
