from django.shortcuts import redirect, render
from django.views.generic import ListView, CreateView, TemplateView
from django.views.generic import View
from .forms import UserCreation, AuthenticationForm
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
# Create your views here.

#here the logic for the system functionality will be applied

"""Class to create a user"""
class CrearUsuario(CreateView):
	#We indicate that the model that we are going to use is the model that django offers
    model = User
	#We indicate that the form to use is the form that we have created in forms.py
    form_class = UserCreation
	#We establish the template to use
    template_name = "registrar.html"
	#We tell you that when the operation has been successfully completed redirect us to the url where you can log in
    success_url = reverse_lazy("login:login")



"""class that is responsible for user authentication to login"""
class Login(FormView):
	#We establish the template to use
	template_name = 'login.html'
	#We indicate that the form to use is the Django authentication form
	form_class = AuthenticationForm
	#We tell you that when the operation has been successfully completed redirect us to the start url of the application
	success_url =  reverse_lazy("product:ListarProductos")

	def form_valid(self, form):
		login(self.request, form.get_user())
		return super(Login, self).form_valid(form)


