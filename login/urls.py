from django.urls import path
from login.views import Login, CrearUsuario
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_view


app_name = "login"

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('Registrar/', CrearUsuario.as_view(), name='registrar'),
    path('logout/', login_required(auth_view.LogoutView.as_view(template_name="login:login")), name='logout')
]