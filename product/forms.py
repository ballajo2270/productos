from django import forms
from django.contrib.auth import models
from django.forms import fields
from .models import Producto


class ProdutoForm(forms.ModelForm):
    """class which is responsible for creating the form to create the products"""

    nombreProduct = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'texts','placeholder':'Nombre del producto'}),
    )
    precio = forms.IntegerField(
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'texts','placeholder':'precio$'}),
    )
    informacion = forms.CharField(
        widget=forms.TextInput(attrs={'class':'form-control', 'type':'texts','placeholder':'informacion'}),
    )
    fecha_adquisicion = forms.DateField(
        widget=forms.DateInput(attrs={'class':'form-control', 'type':'Date','placeholder':'fecha_adquisicion'}),
    )
    
    """Meta class, which is responsible for loading the model you want to use
      and the fields you want to use for the form """

    class Meta:
        model = Producto
        fields = '__all__'
        