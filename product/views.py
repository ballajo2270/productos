from django import forms
import django
from django.contrib.auth import models
from django.http import request
from django.shortcuts import redirect, render
from django.views.generic import ListView, CreateView, TemplateView, DeleteView, UpdateView
from django.views.generic import View
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from .forms import ProdutoForm
from .models import Producto
from openpyxl import Workbook
from django.http.response import HttpResponse

# Create your views here.

#here the logic for the system functionality will be applied

"""this class is in charge of creating the products"""
class CreateProductoView(CreateView):
    model = Producto
    template_name = "CrearProducto.html"
    form_class = ProdutoForm

    """the post method is rewritten, which is responsible
     for capturing the data entered in the template"""
    def post(self, request):
        #the data to be entered is captured
        producto = request.POST.get("producto")
        precio = request.POST.get("precio")
        informacion = request.POST.get("informacion")
        fecha = request.POST.get("fecha")
        img = request.FILES['img']
        #the data captured from the template is assigned to the model field
        instance = Producto(usuario=request.user, nombreProduct=producto, precio=precio, informacion=informacion, fecha_adquisicion=fecha, imagen=img)
        #camputaros fields are saved in the database
        instance.save()
        #We tell him to which page we want to redirect us after saving the data
        return redirect('product:ListarProductos')
       

"""this class serves to bring the data from the database"""
class Listar(ListView):
    model = Producto
    template_name = "ListProduct.html"

    """the get_context_data method is rewritten, which is 
    responsible for bringing the data that we need"""
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #We make the filter so that it only brings us the data of the user who is logged in
        data = Producto.objects.filter(usuario=self.request.user)
        context['object_list'] = data
        return context


    

   




class Eliminar(DeleteView):
    model = Producto
    template_name = "eliminar.html"
    success_url = reverse_lazy('product:ListarProductos') 


class Actualizar(UpdateView):
    model = Producto
    template_name = "ActualizarProducto.html"
    form_class = ProdutoForm
    success_url = reverse_lazy('product:ListarProductos')

    


class Ver(ListView):
    model = Producto
    template_name = "informacion.html"

    def get_queryset(self):
        return Producto.objects.filter(id=self.kwargs['pk'])



class Inicio(TemplateView):
    template_name = "inicio.html"



class Descargar(TemplateView):
    
    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'Reporte de Productos'

        ws.merge_cells('B1:E1')

        ws['B3'] = 'Nombre del Producto'
        ws['C3'] = 'Precio'
        ws['D3'] = 'Informacion'
        ws['E3'] = 'Fecha'

        contador = 4

        for producto in productos:
            ws.cell(row = contador, column=2).value = producto.nombreProduct
            ws.cell(row = contador, column=3).value = producto.precio 
            ws.cell(row = contador, column=4).value = producto.informacion 
            ws.cell(row = contador, column=5).value = producto.fecha_adquisicion 
            contador += 1

        nombre_archivo = 'Productos.xlsx'
        response = HttpResponse(content_type = "application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response

    