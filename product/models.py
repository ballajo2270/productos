from django.db import models
from django.contrib.auth.models import User
# Create your models here.


"""This class is responsible for creating the model to 
upload to the database with its respective fields"""
class Producto(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    nombreProduct = models.CharField(max_length=50, blank=False, null=False)
    precio = models.IntegerField(blank=False, null=False)
    informacion = models.TextField(max_length=500, blank=False, null=False)
    fecha_adquisicion = models.DateField(blank=False, null=False)
    imagen = models.ImageField(upload_to='producto/', null=True, verbose_name='imagen')

    class Meta:
        #db_table is used to give the name you want to the database table
        db_table = 'Productos'


