from django.contrib.auth import logout
from django.urls import path
from product.views import CreateProductoView, Listar, Eliminar, Actualizar, Ver, Inicio, Descargar
from django.contrib.auth.decorators import login_required

app_name = "product"

urlpatterns = [
    path('CrearProductos/', login_required(CreateProductoView.as_view()), name='CrearProductos'),
    path('ListarProductos/', login_required(Listar.as_view()), name='ListarProductos'),
    path('EliminarProducto/<int:pk>/', login_required(Eliminar.as_view()), name='EliminarProducto'),
    path('ActualizarProducto/<int:pk>', login_required(Actualizar.as_view()), name="ActulizarProducto"),
    path('VerProducto/<int:pk>/', login_required(Ver.as_view()), name='VerProducto'),
    path('Descargar/', login_required(Descargar.as_view()), name='Descargar'),
    path('', Inicio.as_view(), name='inicio'),
]